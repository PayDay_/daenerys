namespace DAEnerys
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            Program.settings = null;
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelJointSize = new System.Windows.Forms.Label();
            this.labelZoomSpeed = new System.Windows.Forms.Label();
            this.labelMarkerSize = new System.Windows.Forms.Label();
            this.labelFarClip = new System.Windows.Forms.Label();
            this.numericFarClip = new System.Windows.Forms.NumericUpDown();
            this.numericZoomSpeed = new System.Windows.Forms.NumericUpDown();
            this.groupCamera = new System.Windows.Forms.GroupBox();
            this.checkSmoothZooming = new System.Windows.Forms.CheckBox();
            this.labelFOV = new System.Windows.Forms.Label();
            this.numericFOV = new System.Windows.Forms.NumericUpDown();
            this.labelNearClip = new System.Windows.Forms.Label();
            this.numericNearClip = new System.Windows.Forms.NumericUpDown();
            this.groupEditor = new System.Windows.Forms.GroupBox();
            this.labelRotationIncrement = new System.Windows.Forms.Label();
            this.numericRotationIncrement = new System.Windows.Forms.NumericUpDown();
            this.labelPositionIncrement = new System.Windows.Forms.Label();
            this.numericPositionIncrement = new System.Windows.Forms.NumericUpDown();
            this.labelIconSize = new System.Windows.Forms.Label();
            this.numericIconSize = new System.Windows.Forms.NumericUpDown();
            this.numericMarkerSize = new System.Windows.Forms.NumericUpDown();
            this.numericJointSize = new System.Windows.Forms.NumericUpDown();
            this.buttonBackgroundColor = new System.Windows.Forms.Button();
            this.labelBackgroundColor = new System.Windows.Forms.Label();
            this.buttonAmbientColor = new System.Windows.Forms.Button();
            this.labelAmbientColor = new System.Windows.Forms.Label();
            this.groupDataPaths = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRemoveDataPath = new System.Windows.Forms.Button();
            this.buttonAddDataPath = new System.Windows.Forms.Button();
            this.listDataPaths = new System.Windows.Forms.ListBox();
            this.addDataPathDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupRendering = new System.Windows.Forms.GroupBox();
            this.comboBackground = new System.Windows.Forms.ComboBox();
            this.labelBackground = new System.Windows.Forms.Label();
            this.checkDisableLighting = new System.Windows.Forms.CheckBox();
            this.checkVSync = new System.Windows.Forms.CheckBox();
            this.checkRenderOnTop = new System.Windows.Forms.CheckBox();
            this.labelFSAASamples = new System.Windows.Forms.Label();
            this.comboFSAASamples = new System.Windows.Forms.ComboBox();
            this.groupRace = new System.Windows.Forms.GroupBox();
            this.engineColorButton5 = new DAEnerys.EngineColorButton();
            this.engineColorButton4 = new DAEnerys.EngineColorButton();
            this.engineColorButton3 = new DAEnerys.EngineColorButton();
            this.engineColorButton2 = new DAEnerys.EngineColorButton();
            this.engineColorButtonCustom = new DAEnerys.EngineColorButton();
            this.engineColorButton1 = new DAEnerys.EngineColorButton();
            this.engineColorButton0 = new DAEnerys.EngineColorButton();
            this.labelEngineColor = new System.Windows.Forms.Label();
            this.buttonEngineColor = new System.Windows.Forms.Button();
            this.labelBadge = new System.Windows.Forms.Label();
            this.comboBadge = new System.Windows.Forms.ComboBox();
            this.teamColorButton20 = new DAEnerys.TeamColorButton();
            this.teamColorButton19 = new DAEnerys.TeamColorButton();
            this.teamColorButton18 = new DAEnerys.TeamColorButton();
            this.teamColorButton17 = new DAEnerys.TeamColorButton();
            this.teamColorButton16 = new DAEnerys.TeamColorButton();
            this.teamColorButton15 = new DAEnerys.TeamColorButton();
            this.teamColorButton14 = new DAEnerys.TeamColorButton();
            this.teamColorButton13 = new DAEnerys.TeamColorButton();
            this.teamColorButton12 = new DAEnerys.TeamColorButton();
            this.teamColorButton11 = new DAEnerys.TeamColorButton();
            this.buttonTeamColorSwap = new System.Windows.Forms.Button();
            this.teamColorButtonCustom = new DAEnerys.TeamColorButton();
            this.teamColorButtonDefault = new DAEnerys.TeamColorButton();
            this.teamColorButton10 = new DAEnerys.TeamColorButton();
            this.teamColorButton9 = new DAEnerys.TeamColorButton();
            this.teamColorButton8 = new DAEnerys.TeamColorButton();
            this.teamColorButton7 = new DAEnerys.TeamColorButton();
            this.teamColorButton6 = new DAEnerys.TeamColorButton();
            this.teamColorButton5 = new DAEnerys.TeamColorButton();
            this.teamColorButton4 = new DAEnerys.TeamColorButton();
            this.teamColorButton3 = new DAEnerys.TeamColorButton();
            this.teamColorButton2 = new DAEnerys.TeamColorButton();
            this.teamColorButton1 = new DAEnerys.TeamColorButton();
            this.buttonStripeColor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonTeamColor = new System.Windows.Forms.Button();
            this.teamColorButton0 = new DAEnerys.TeamColorButton();
            this.label3 = new System.Windows.Forms.Label();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.groupUpdates = new System.Windows.Forms.GroupBox();
            this.checkCheckForUpdates = new System.Windows.Forms.CheckBox();
            this.colorDialogAlpha = new Opulos.Core.UI.AlphaColorDialog();
            this.labelDockpathSize = new System.Windows.Forms.Label();
            this.numericDockpathSize = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericFarClip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericZoomSpeed)).BeginInit();
            this.groupCamera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFOV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNearClip)).BeginInit();
            this.groupEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRotationIncrement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPositionIncrement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMarkerSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJointSize)).BeginInit();
            this.groupDataPaths.SuspendLayout();
            this.groupRendering.SuspendLayout();
            this.groupRace.SuspendLayout();
            this.groupUpdates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDockpathSize)).BeginInit();
            this.SuspendLayout();
            // 
            // labelJointSize
            // 
            this.labelJointSize.AutoSize = true;
            this.labelJointSize.Location = new System.Drawing.Point(8, 21);
            this.labelJointSize.Name = "labelJointSize";
            this.labelJointSize.Size = new System.Drawing.Size(50, 13);
            this.labelJointSize.TabIndex = 0;
            this.labelJointSize.Text = "Joint size";
            // 
            // labelZoomSpeed
            // 
            this.labelZoomSpeed.AutoSize = true;
            this.labelZoomSpeed.Location = new System.Drawing.Point(8, 21);
            this.labelZoomSpeed.Name = "labelZoomSpeed";
            this.labelZoomSpeed.Size = new System.Drawing.Size(66, 13);
            this.labelZoomSpeed.TabIndex = 3;
            this.labelZoomSpeed.Text = "Zoom speed";
            // 
            // labelMarkerSize
            // 
            this.labelMarkerSize.AutoSize = true;
            this.labelMarkerSize.Location = new System.Drawing.Point(8, 47);
            this.labelMarkerSize.Name = "labelMarkerSize";
            this.labelMarkerSize.Size = new System.Drawing.Size(61, 13);
            this.labelMarkerSize.TabIndex = 7;
            this.labelMarkerSize.Text = "Marker size";
            // 
            // labelFarClip
            // 
            this.labelFarClip.AutoSize = true;
            this.labelFarClip.Location = new System.Drawing.Point(8, 47);
            this.labelFarClip.Name = "labelFarClip";
            this.labelFarClip.Size = new System.Drawing.Size(41, 13);
            this.labelFarClip.TabIndex = 10;
            this.labelFarClip.Text = "Far clip";
            // 
            // numericFarClip
            // 
            this.numericFarClip.DecimalPlaces = 4;
            this.numericFarClip.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericFarClip.Location = new System.Drawing.Point(105, 45);
            this.numericFarClip.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericFarClip.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numericFarClip.Name = "numericFarClip";
            this.numericFarClip.Size = new System.Drawing.Size(218, 20);
            this.numericFarClip.TabIndex = 11;
            this.numericFarClip.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericFarClip.ValueChanged += new System.EventHandler(this.numericClipDistance_ValueChanged);
            // 
            // numericZoomSpeed
            // 
            this.numericZoomSpeed.DecimalPlaces = 1;
            this.numericZoomSpeed.Location = new System.Drawing.Point(105, 19);
            this.numericZoomSpeed.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericZoomSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            524288});
            this.numericZoomSpeed.Name = "numericZoomSpeed";
            this.numericZoomSpeed.Size = new System.Drawing.Size(218, 20);
            this.numericZoomSpeed.TabIndex = 12;
            this.numericZoomSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericZoomSpeed.ValueChanged += new System.EventHandler(this.numericZoomSpeed_ValueChanged);
            // 
            // groupCamera
            // 
            this.groupCamera.AutoSize = true;
            this.groupCamera.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupCamera.Controls.Add(this.checkSmoothZooming);
            this.groupCamera.Controls.Add(this.labelFOV);
            this.groupCamera.Controls.Add(this.numericFOV);
            this.groupCamera.Controls.Add(this.labelNearClip);
            this.groupCamera.Controls.Add(this.numericNearClip);
            this.groupCamera.Controls.Add(this.labelFarClip);
            this.groupCamera.Controls.Add(this.labelZoomSpeed);
            this.groupCamera.Controls.Add(this.numericZoomSpeed);
            this.groupCamera.Controls.Add(this.numericFarClip);
            this.groupCamera.Location = new System.Drawing.Point(8, 8);
            this.groupCamera.Name = "groupCamera";
            this.groupCamera.Size = new System.Drawing.Size(329, 160);
            this.groupCamera.TabIndex = 15;
            this.groupCamera.TabStop = false;
            this.groupCamera.Text = "Camera";
            // 
            // checkSmoothZooming
            // 
            this.checkSmoothZooming.AutoSize = true;
            this.checkSmoothZooming.Checked = true;
            this.checkSmoothZooming.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkSmoothZooming.Location = new System.Drawing.Point(11, 124);
            this.checkSmoothZooming.Name = "checkSmoothZooming";
            this.checkSmoothZooming.Size = new System.Drawing.Size(104, 17);
            this.checkSmoothZooming.TabIndex = 26;
            this.checkSmoothZooming.Text = "Smooth zooming";
            this.checkSmoothZooming.UseVisualStyleBackColor = true;
            this.checkSmoothZooming.CheckedChanged += new System.EventHandler(this.checkSmoothZooming_CheckedChanged);
            // 
            // labelFOV
            // 
            this.labelFOV.AutoSize = true;
            this.labelFOV.Location = new System.Drawing.Point(8, 99);
            this.labelFOV.Name = "labelFOV";
            this.labelFOV.Size = new System.Drawing.Size(66, 13);
            this.labelFOV.TabIndex = 16;
            this.labelFOV.Text = "Field of view";
            // 
            // numericFOV
            // 
            this.numericFOV.Location = new System.Drawing.Point(105, 97);
            this.numericFOV.Maximum = new decimal(new int[] {
            179,
            0,
            0,
            0});
            this.numericFOV.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericFOV.Name = "numericFOV";
            this.numericFOV.Size = new System.Drawing.Size(218, 20);
            this.numericFOV.TabIndex = 15;
            this.numericFOV.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericFOV.ValueChanged += new System.EventHandler(this.numericFOV_ValueChanged);
            // 
            // labelNearClip
            // 
            this.labelNearClip.AutoSize = true;
            this.labelNearClip.Location = new System.Drawing.Point(8, 73);
            this.labelNearClip.Name = "labelNearClip";
            this.labelNearClip.Size = new System.Drawing.Size(49, 13);
            this.labelNearClip.TabIndex = 14;
            this.labelNearClip.Text = "Near clip";
            // 
            // numericNearClip
            // 
            this.numericNearClip.DecimalPlaces = 4;
            this.numericNearClip.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericNearClip.Location = new System.Drawing.Point(105, 71);
            this.numericNearClip.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericNearClip.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numericNearClip.Name = "numericNearClip";
            this.numericNearClip.Size = new System.Drawing.Size(218, 20);
            this.numericNearClip.TabIndex = 13;
            this.numericNearClip.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericNearClip.ValueChanged += new System.EventHandler(this.numericNearClip_ValueChanged);
            // 
            // groupEditor
            // 
            this.groupEditor.AutoSize = true;
            this.groupEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupEditor.Controls.Add(this.labelDockpathSize);
            this.groupEditor.Controls.Add(this.numericDockpathSize);
            this.groupEditor.Controls.Add(this.labelRotationIncrement);
            this.groupEditor.Controls.Add(this.numericRotationIncrement);
            this.groupEditor.Controls.Add(this.labelPositionIncrement);
            this.groupEditor.Controls.Add(this.numericPositionIncrement);
            this.groupEditor.Controls.Add(this.labelIconSize);
            this.groupEditor.Controls.Add(this.numericIconSize);
            this.groupEditor.Controls.Add(this.numericMarkerSize);
            this.groupEditor.Controls.Add(this.numericJointSize);
            this.groupEditor.Controls.Add(this.labelMarkerSize);
            this.groupEditor.Controls.Add(this.labelJointSize);
            this.groupEditor.Location = new System.Drawing.Point(8, 180);
            this.groupEditor.Name = "groupEditor";
            this.groupEditor.Size = new System.Drawing.Size(329, 206);
            this.groupEditor.TabIndex = 16;
            this.groupEditor.TabStop = false;
            this.groupEditor.Text = "Editor";
            // 
            // labelRotationIncrement
            // 
            this.labelRotationIncrement.AutoSize = true;
            this.labelRotationIncrement.Location = new System.Drawing.Point(8, 169);
            this.labelRotationIncrement.Name = "labelRotationIncrement";
            this.labelRotationIncrement.Size = new System.Drawing.Size(96, 13);
            this.labelRotationIncrement.TabIndex = 25;
            this.labelRotationIncrement.Text = "Rotation increment";
            // 
            // numericRotationIncrement
            // 
            this.numericRotationIncrement.DecimalPlaces = 3;
            this.numericRotationIncrement.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericRotationIncrement.Location = new System.Drawing.Point(105, 167);
            this.numericRotationIncrement.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericRotationIncrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericRotationIncrement.Name = "numericRotationIncrement";
            this.numericRotationIncrement.Size = new System.Drawing.Size(218, 20);
            this.numericRotationIncrement.TabIndex = 24;
            this.numericRotationIncrement.Value = new decimal(new int[] {
            45,
            0,
            0,
            65536});
            this.numericRotationIncrement.ValueChanged += new System.EventHandler(this.numericRotationIncrement_ValueChanged);
            // 
            // labelPositionIncrement
            // 
            this.labelPositionIncrement.AutoSize = true;
            this.labelPositionIncrement.Location = new System.Drawing.Point(8, 143);
            this.labelPositionIncrement.Name = "labelPositionIncrement";
            this.labelPositionIncrement.Size = new System.Drawing.Size(93, 13);
            this.labelPositionIncrement.TabIndex = 21;
            this.labelPositionIncrement.Text = "Position increment";
            // 
            // numericPositionIncrement
            // 
            this.numericPositionIncrement.DecimalPlaces = 3;
            this.numericPositionIncrement.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericPositionIncrement.Location = new System.Drawing.Point(105, 141);
            this.numericPositionIncrement.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericPositionIncrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericPositionIncrement.Name = "numericPositionIncrement";
            this.numericPositionIncrement.Size = new System.Drawing.Size(218, 20);
            this.numericPositionIncrement.TabIndex = 20;
            this.numericPositionIncrement.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericPositionIncrement.ValueChanged += new System.EventHandler(this.numericPositionIncrement_ValueChanged);
            // 
            // labelIconSize
            // 
            this.labelIconSize.AutoSize = true;
            this.labelIconSize.Location = new System.Drawing.Point(8, 73);
            this.labelIconSize.Name = "labelIconSize";
            this.labelIconSize.Size = new System.Drawing.Size(49, 13);
            this.labelIconSize.TabIndex = 19;
            this.labelIconSize.Text = "Icon size";
            // 
            // numericIconSize
            // 
            this.numericIconSize.DecimalPlaces = 3;
            this.numericIconSize.Location = new System.Drawing.Point(105, 71);
            this.numericIconSize.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericIconSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericIconSize.Name = "numericIconSize";
            this.numericIconSize.Size = new System.Drawing.Size(218, 20);
            this.numericIconSize.TabIndex = 18;
            this.numericIconSize.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericIconSize.ValueChanged += new System.EventHandler(this.numericIconSize_ValueChanged);
            // 
            // numericMarkerSize
            // 
            this.numericMarkerSize.DecimalPlaces = 3;
            this.numericMarkerSize.Location = new System.Drawing.Point(105, 45);
            this.numericMarkerSize.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericMarkerSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            524288});
            this.numericMarkerSize.Name = "numericMarkerSize";
            this.numericMarkerSize.Size = new System.Drawing.Size(218, 20);
            this.numericMarkerSize.TabIndex = 16;
            this.numericMarkerSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericMarkerSize.ValueChanged += new System.EventHandler(this.numericMarkerSize_ValueChanged);
            // 
            // numericJointSize
            // 
            this.numericJointSize.DecimalPlaces = 1;
            this.numericJointSize.Location = new System.Drawing.Point(105, 19);
            this.numericJointSize.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericJointSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            524288});
            this.numericJointSize.Name = "numericJointSize";
            this.numericJointSize.Size = new System.Drawing.Size(218, 20);
            this.numericJointSize.TabIndex = 15;
            this.numericJointSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericJointSize.ValueChanged += new System.EventHandler(this.numericJointSize_ValueChanged);
            // 
            // buttonBackgroundColor
            // 
            this.buttonBackgroundColor.BackColor = System.Drawing.Color.Red;
            this.buttonBackgroundColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackgroundColor.Location = new System.Drawing.Point(104, 144);
            this.buttonBackgroundColor.Name = "buttonBackgroundColor";
            this.buttonBackgroundColor.Size = new System.Drawing.Size(232, 20);
            this.buttonBackgroundColor.TabIndex = 4;
            this.buttonBackgroundColor.UseVisualStyleBackColor = false;
            this.buttonBackgroundColor.Click += new System.EventHandler(this.buttonBackgroundColor_Click);
            // 
            // labelBackgroundColor
            // 
            this.labelBackgroundColor.AutoSize = true;
            this.labelBackgroundColor.Location = new System.Drawing.Point(7, 148);
            this.labelBackgroundColor.Name = "labelBackgroundColor";
            this.labelBackgroundColor.Size = new System.Drawing.Size(91, 13);
            this.labelBackgroundColor.TabIndex = 3;
            this.labelBackgroundColor.Text = "Background color";
            // 
            // buttonAmbientColor
            // 
            this.buttonAmbientColor.BackColor = System.Drawing.Color.Red;
            this.buttonAmbientColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAmbientColor.Location = new System.Drawing.Point(104, 120);
            this.buttonAmbientColor.Name = "buttonAmbientColor";
            this.buttonAmbientColor.Size = new System.Drawing.Size(232, 20);
            this.buttonAmbientColor.TabIndex = 2;
            this.buttonAmbientColor.UseVisualStyleBackColor = false;
            this.buttonAmbientColor.Click += new System.EventHandler(this.buttonAmbientColor_Click);
            // 
            // labelAmbientColor
            // 
            this.labelAmbientColor.AutoSize = true;
            this.labelAmbientColor.Location = new System.Drawing.Point(7, 124);
            this.labelAmbientColor.Name = "labelAmbientColor";
            this.labelAmbientColor.Size = new System.Drawing.Size(71, 13);
            this.labelAmbientColor.TabIndex = 0;
            this.labelAmbientColor.Text = "Ambient color";
            // 
            // groupDataPaths
            // 
            this.groupDataPaths.AutoSize = true;
            this.groupDataPaths.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupDataPaths.Controls.Add(this.label1);
            this.groupDataPaths.Controls.Add(this.buttonRemoveDataPath);
            this.groupDataPaths.Controls.Add(this.buttonAddDataPath);
            this.groupDataPaths.Controls.Add(this.listDataPaths);
            this.groupDataPaths.Location = new System.Drawing.Point(8, 392);
            this.groupDataPaths.Name = "groupDataPaths";
            this.groupDataPaths.Size = new System.Drawing.Size(329, 188);
            this.groupDataPaths.TabIndex = 19;
            this.groupDataPaths.TabStop = false;
            this.groupDataPaths.Text = "Data paths";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(317, 26);
            this.label1.TabIndex = 25;
            this.label1.Text = "The order of the paths matter. Files in lower paths will overwrite files in the p" +
    "aths above them.";
            // 
            // buttonRemoveDataPath
            // 
            this.buttonRemoveDataPath.Location = new System.Drawing.Point(168, 120);
            this.buttonRemoveDataPath.Name = "buttonRemoveDataPath";
            this.buttonRemoveDataPath.Size = new System.Drawing.Size(155, 23);
            this.buttonRemoveDataPath.TabIndex = 24;
            this.buttonRemoveDataPath.Text = "Remove";
            this.buttonRemoveDataPath.UseVisualStyleBackColor = true;
            this.buttonRemoveDataPath.Click += new System.EventHandler(this.buttonRemoveDataPath_Click);
            // 
            // buttonAddDataPath
            // 
            this.buttonAddDataPath.Location = new System.Drawing.Point(6, 120);
            this.buttonAddDataPath.Name = "buttonAddDataPath";
            this.buttonAddDataPath.Size = new System.Drawing.Size(156, 23);
            this.buttonAddDataPath.TabIndex = 23;
            this.buttonAddDataPath.Text = "Add";
            this.buttonAddDataPath.UseVisualStyleBackColor = true;
            this.buttonAddDataPath.Click += new System.EventHandler(this.buttonAddDataPath_Click);
            // 
            // listDataPaths
            // 
            this.listDataPaths.FormattingEnabled = true;
            this.listDataPaths.HorizontalScrollbar = true;
            this.listDataPaths.Location = new System.Drawing.Point(6, 19);
            this.listDataPaths.Name = "listDataPaths";
            this.listDataPaths.Size = new System.Drawing.Size(317, 95);
            this.listDataPaths.TabIndex = 22;
            // 
            // addDataPathDialog
            // 
            this.addDataPathDialog.FileName = "keeper.txt";
            this.addDataPathDialog.Filter = "Data roots|keeper.txt";
            this.addDataPathDialog.Title = "Select keeper.txt in data root folder";
            // 
            // groupRendering
            // 
            this.groupRendering.AutoSize = true;
            this.groupRendering.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupRendering.Controls.Add(this.comboBackground);
            this.groupRendering.Controls.Add(this.labelBackground);
            this.groupRendering.Controls.Add(this.buttonBackgroundColor);
            this.groupRendering.Controls.Add(this.checkDisableLighting);
            this.groupRendering.Controls.Add(this.labelBackgroundColor);
            this.groupRendering.Controls.Add(this.checkVSync);
            this.groupRendering.Controls.Add(this.buttonAmbientColor);
            this.groupRendering.Controls.Add(this.labelAmbientColor);
            this.groupRendering.Controls.Add(this.checkRenderOnTop);
            this.groupRendering.Controls.Add(this.labelFSAASamples);
            this.groupRendering.Controls.Add(this.comboFSAASamples);
            this.groupRendering.Location = new System.Drawing.Point(352, 8);
            this.groupRendering.Name = "groupRendering";
            this.groupRendering.Size = new System.Drawing.Size(342, 208);
            this.groupRendering.TabIndex = 18;
            this.groupRendering.TabStop = false;
            this.groupRendering.Text = "Rendering";
            // 
            // comboBackground
            // 
            this.comboBackground.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBackground.FormattingEnabled = true;
            this.comboBackground.Location = new System.Drawing.Point(104, 168);
            this.comboBackground.Name = "comboBackground";
            this.comboBackground.Size = new System.Drawing.Size(232, 21);
            this.comboBackground.TabIndex = 27;
            this.comboBackground.SelectedIndexChanged += new System.EventHandler(this.comboBackground_SelectedIndexChanged);
            // 
            // labelBackground
            // 
            this.labelBackground.AutoSize = true;
            this.labelBackground.Location = new System.Drawing.Point(7, 172);
            this.labelBackground.Name = "labelBackground";
            this.labelBackground.Size = new System.Drawing.Size(65, 13);
            this.labelBackground.TabIndex = 26;
            this.labelBackground.Text = "Background";
            // 
            // checkDisableLighting
            // 
            this.checkDisableLighting.AutoSize = true;
            this.checkDisableLighting.Location = new System.Drawing.Point(11, 92);
            this.checkDisableLighting.Name = "checkDisableLighting";
            this.checkDisableLighting.Size = new System.Drawing.Size(97, 17);
            this.checkDisableLighting.TabIndex = 25;
            this.checkDisableLighting.Text = "Disable lighting";
            this.checkDisableLighting.UseVisualStyleBackColor = true;
            this.checkDisableLighting.CheckedChanged += new System.EventHandler(this.checkDisableLighting_CheckedChanged);
            // 
            // checkVSync
            // 
            this.checkVSync.AutoSize = true;
            this.checkVSync.Checked = true;
            this.checkVSync.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkVSync.Location = new System.Drawing.Point(11, 69);
            this.checkVSync.Name = "checkVSync";
            this.checkVSync.Size = new System.Drawing.Size(172, 17);
            this.checkVSync.TabIndex = 23;
            this.checkVSync.Text = "Enable vertical synchronization";
            this.checkVSync.UseVisualStyleBackColor = true;
            this.checkVSync.CheckedChanged += new System.EventHandler(this.checkVSync_CheckedChanged);
            // 
            // checkRenderOnTop
            // 
            this.checkRenderOnTop.AutoSize = true;
            this.checkRenderOnTop.Checked = true;
            this.checkRenderOnTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkRenderOnTop.Location = new System.Drawing.Point(11, 46);
            this.checkRenderOnTop.Name = "checkRenderOnTop";
            this.checkRenderOnTop.Size = new System.Drawing.Size(151, 17);
            this.checkRenderOnTop.TabIndex = 21;
            this.checkRenderOnTop.Text = "Draw visualizations in front";
            this.checkRenderOnTop.UseVisualStyleBackColor = true;
            this.checkRenderOnTop.CheckedChanged += new System.EventHandler(this.checkRenderOnTop_CheckedChanged);
            // 
            // labelFSAASamples
            // 
            this.labelFSAASamples.AutoSize = true;
            this.labelFSAASamples.Location = new System.Drawing.Point(7, 22);
            this.labelFSAASamples.Name = "labelFSAASamples";
            this.labelFSAASamples.Size = new System.Drawing.Size(92, 13);
            this.labelFSAASamples.TabIndex = 20;
            this.labelFSAASamples.Text = "FSAA anti-aliasing";
            // 
            // comboFSAASamples
            // 
            this.comboFSAASamples.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFSAASamples.FormattingEnabled = true;
            this.comboFSAASamples.Items.AddRange(new object[] {
            "0 samples",
            "2 samples",
            "4 samples"});
            this.comboFSAASamples.Location = new System.Drawing.Point(104, 19);
            this.comboFSAASamples.Name = "comboFSAASamples";
            this.comboFSAASamples.Size = new System.Drawing.Size(232, 21);
            this.comboFSAASamples.TabIndex = 19;
            this.comboFSAASamples.SelectedIndexChanged += new System.EventHandler(this.comboFSAASamples_SelectedIndexChanged);
            // 
            // groupRace
            // 
            this.groupRace.AutoSize = true;
            this.groupRace.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupRace.Controls.Add(this.engineColorButton5);
            this.groupRace.Controls.Add(this.engineColorButton4);
            this.groupRace.Controls.Add(this.engineColorButton3);
            this.groupRace.Controls.Add(this.engineColorButton2);
            this.groupRace.Controls.Add(this.engineColorButtonCustom);
            this.groupRace.Controls.Add(this.engineColorButton1);
            this.groupRace.Controls.Add(this.engineColorButton0);
            this.groupRace.Controls.Add(this.labelEngineColor);
            this.groupRace.Controls.Add(this.buttonEngineColor);
            this.groupRace.Controls.Add(this.labelBadge);
            this.groupRace.Controls.Add(this.comboBadge);
            this.groupRace.Controls.Add(this.teamColorButton20);
            this.groupRace.Controls.Add(this.teamColorButton19);
            this.groupRace.Controls.Add(this.teamColorButton18);
            this.groupRace.Controls.Add(this.teamColorButton17);
            this.groupRace.Controls.Add(this.teamColorButton16);
            this.groupRace.Controls.Add(this.teamColorButton15);
            this.groupRace.Controls.Add(this.teamColorButton14);
            this.groupRace.Controls.Add(this.teamColorButton13);
            this.groupRace.Controls.Add(this.teamColorButton12);
            this.groupRace.Controls.Add(this.teamColorButton11);
            this.groupRace.Controls.Add(this.buttonTeamColorSwap);
            this.groupRace.Controls.Add(this.teamColorButtonCustom);
            this.groupRace.Controls.Add(this.teamColorButtonDefault);
            this.groupRace.Controls.Add(this.teamColorButton10);
            this.groupRace.Controls.Add(this.teamColorButton9);
            this.groupRace.Controls.Add(this.teamColorButton8);
            this.groupRace.Controls.Add(this.teamColorButton7);
            this.groupRace.Controls.Add(this.teamColorButton6);
            this.groupRace.Controls.Add(this.teamColorButton5);
            this.groupRace.Controls.Add(this.teamColorButton4);
            this.groupRace.Controls.Add(this.teamColorButton3);
            this.groupRace.Controls.Add(this.teamColorButton2);
            this.groupRace.Controls.Add(this.teamColorButton1);
            this.groupRace.Controls.Add(this.buttonStripeColor);
            this.groupRace.Controls.Add(this.label2);
            this.groupRace.Controls.Add(this.buttonTeamColor);
            this.groupRace.Controls.Add(this.teamColorButton0);
            this.groupRace.Controls.Add(this.label3);
            this.groupRace.Location = new System.Drawing.Point(352, 216);
            this.groupRace.Name = "groupRace";
            this.groupRace.Size = new System.Drawing.Size(343, 236);
            this.groupRace.TabIndex = 20;
            this.groupRace.TabStop = false;
            this.groupRace.Text = "Race";
            // 
            // engineColorButton5
            // 
            this.engineColorButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton5.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(242)))), ((int)(((byte)(89)))), ((int)(((byte)(46)))));
            this.engineColorButton5.Location = new System.Drawing.Point(196, 197);
            this.engineColorButton5.Name = "engineColorButton5";
            this.engineColorButton5.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton5.TabIndex = 164;
            this.engineColorButton5.UsePreset = DAEnerys.EngineColorButton.PresetColors.Kadeshi;
            this.engineColorButton5.UseVisualStyleBackColor = false;
            this.engineColorButton5.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButton4
            // 
            this.engineColorButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton4.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.engineColorButton4.Location = new System.Drawing.Point(172, 197);
            this.engineColorButton4.Name = "engineColorButton4";
            this.engineColorButton4.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton4.TabIndex = 163;
            this.engineColorButton4.UsePreset = DAEnerys.EngineColorButton.PresetColors.Progenitor;
            this.engineColorButton4.UseVisualStyleBackColor = false;
            this.engineColorButton4.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButton3
            // 
            this.engineColorButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton3.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(120)))), ((int)(((byte)(176)))));
            this.engineColorButton3.Location = new System.Drawing.Point(148, 197);
            this.engineColorButton3.Name = "engineColorButton3";
            this.engineColorButton3.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton3.TabIndex = 162;
            this.engineColorButton3.UsePreset = DAEnerys.EngineColorButton.PresetColors.Taiidan;
            this.engineColorButton3.UseVisualStyleBackColor = false;
            this.engineColorButton3.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButton2
            // 
            this.engineColorButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton2.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(179)))), ((int)(((byte)(140)))), ((int)(((byte)(89)))));
            this.engineColorButton2.Location = new System.Drawing.Point(124, 197);
            this.engineColorButton2.Name = "engineColorButton2";
            this.engineColorButton2.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton2.TabIndex = 161;
            this.engineColorButton2.UsePreset = DAEnerys.EngineColorButton.PresetColors.Kushan;
            this.engineColorButton2.UseVisualStyleBackColor = false;
            this.engineColorButton2.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButtonCustom
            // 
            this.engineColorButtonCustom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(120)))), ((int)(((byte)(176)))));
            this.engineColorButtonCustom.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(120)))), ((int)(((byte)(176)))));
            this.engineColorButtonCustom.Location = new System.Drawing.Point(244, 197);
            this.engineColorButtonCustom.Name = "engineColorButtonCustom";
            this.engineColorButtonCustom.Size = new System.Drawing.Size(20, 20);
            this.engineColorButtonCustom.TabIndex = 160;
            this.engineColorButtonCustom.UsePreset = DAEnerys.EngineColorButton.PresetColors.Custom;
            this.engineColorButtonCustom.UseVisualStyleBackColor = false;
            this.engineColorButtonCustom.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButton1
            // 
            this.engineColorButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton1.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(235)))), ((int)(((byte)(54)))), ((int)(((byte)(23)))));
            this.engineColorButton1.Location = new System.Drawing.Point(100, 197);
            this.engineColorButton1.Name = "engineColorButton1";
            this.engineColorButton1.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton1.TabIndex = 159;
            this.engineColorButton1.UsePreset = DAEnerys.EngineColorButton.PresetColors.Vaygr;
            this.engineColorButton1.UseVisualStyleBackColor = false;
            this.engineColorButton1.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // engineColorButton0
            // 
            this.engineColorButton0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(120)))), ((int)(((byte)(176)))));
            this.engineColorButton0.EngineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(120)))), ((int)(((byte)(176)))));
            this.engineColorButton0.Location = new System.Drawing.Point(76, 197);
            this.engineColorButton0.Name = "engineColorButton0";
            this.engineColorButton0.Size = new System.Drawing.Size(20, 20);
            this.engineColorButton0.TabIndex = 158;
            this.engineColorButton0.UsePreset = DAEnerys.EngineColorButton.PresetColors.Hiigaran;
            this.engineColorButton0.UseVisualStyleBackColor = false;
            this.engineColorButton0.Click += new System.EventHandler(this.buttonEngineColorPreset_Click);
            // 
            // labelEngineColor
            // 
            this.labelEngineColor.AutoSize = true;
            this.labelEngineColor.Location = new System.Drawing.Point(8, 175);
            this.labelEngineColor.Name = "labelEngineColor";
            this.labelEngineColor.Size = new System.Drawing.Size(66, 13);
            this.labelEngineColor.TabIndex = 157;
            this.labelEngineColor.Text = "Engine color";
            // 
            // buttonEngineColor
            // 
            this.buttonEngineColor.BackColor = System.Drawing.Color.Red;
            this.buttonEngineColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEngineColor.Location = new System.Drawing.Point(76, 171);
            this.buttonEngineColor.Name = "buttonEngineColor";
            this.buttonEngineColor.Size = new System.Drawing.Size(261, 20);
            this.buttonEngineColor.TabIndex = 156;
            this.buttonEngineColor.UseVisualStyleBackColor = false;
            this.buttonEngineColor.Click += new System.EventHandler(this.buttonEngineColor_Click);
            // 
            // labelBadge
            // 
            this.labelBadge.AutoSize = true;
            this.labelBadge.Location = new System.Drawing.Point(8, 147);
            this.labelBadge.Name = "labelBadge";
            this.labelBadge.Size = new System.Drawing.Size(38, 13);
            this.labelBadge.TabIndex = 155;
            this.labelBadge.Text = "Badge";
            // 
            // comboBadge
            // 
            this.comboBadge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBadge.FormattingEnabled = true;
            this.comboBadge.Location = new System.Drawing.Point(76, 144);
            this.comboBadge.Name = "comboBadge";
            this.comboBadge.Size = new System.Drawing.Size(261, 21);
            this.comboBadge.TabIndex = 154;
            this.comboBadge.SelectedIndexChanged += new System.EventHandler(this.comboBadge_SelectedIndexChanged);
            // 
            // teamColorButton20
            // 
            this.teamColorButton20.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton20.Location = new System.Drawing.Point(196, 118);
            this.teamColorButton20.Name = "teamColorButton20";
            this.teamColorButton20.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton20.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(153)))));
            this.teamColorButton20.TabIndex = 153;
            this.teamColorButton20.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.teamColorButton20.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP7;
            this.teamColorButton20.UseVisualStyleBackColor = false;
            this.teamColorButton20.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton19
            // 
            this.teamColorButton19.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton19.Location = new System.Drawing.Point(172, 118);
            this.teamColorButton19.Name = "teamColorButton19";
            this.teamColorButton19.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton19.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))));
            this.teamColorButton19.TabIndex = 152;
            this.teamColorButton19.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(127)))));
            this.teamColorButton19.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP6;
            this.teamColorButton19.UseVisualStyleBackColor = false;
            this.teamColorButton19.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton18
            // 
            this.teamColorButton18.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton18.Location = new System.Drawing.Point(148, 118);
            this.teamColorButton18.Name = "teamColorButton18";
            this.teamColorButton18.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton18.StripeColor = System.Drawing.Color.Yellow;
            this.teamColorButton18.TabIndex = 151;
            this.teamColorButton18.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(129)))), ((int)(((byte)(182)))));
            this.teamColorButton18.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP5;
            this.teamColorButton18.UseVisualStyleBackColor = false;
            this.teamColorButton18.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton17
            // 
            this.teamColorButton17.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton17.Location = new System.Drawing.Point(124, 118);
            this.teamColorButton17.Name = "teamColorButton17";
            this.teamColorButton17.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton17.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(211)))), ((int)(((byte)(0)))));
            this.teamColorButton17.TabIndex = 150;
            this.teamColorButton17.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.teamColorButton17.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP4;
            this.teamColorButton17.UseVisualStyleBackColor = false;
            this.teamColorButton17.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton16
            // 
            this.teamColorButton16.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton16.Location = new System.Drawing.Point(100, 118);
            this.teamColorButton16.Name = "teamColorButton16";
            this.teamColorButton16.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton16.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.teamColorButton16.TabIndex = 149;
            this.teamColorButton16.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(255)))), ((int)(((byte)(233)))));
            this.teamColorButton16.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP3;
            this.teamColorButton16.UseVisualStyleBackColor = false;
            this.teamColorButton16.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton15
            // 
            this.teamColorButton15.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton15.Location = new System.Drawing.Point(76, 118);
            this.teamColorButton15.Name = "teamColorButton15";
            this.teamColorButton15.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton15.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.teamColorButton15.TabIndex = 148;
            this.teamColorButton15.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(229)))), ((int)(((byte)(7)))));
            this.teamColorButton15.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP2;
            this.teamColorButton15.UseVisualStyleBackColor = false;
            this.teamColorButton15.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton14
            // 
            this.teamColorButton14.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton14.Location = new System.Drawing.Point(244, 94);
            this.teamColorButton14.Name = "teamColorButton14";
            this.teamColorButton14.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton14.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.teamColorButton14.TabIndex = 147;
            this.teamColorButton14.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(165)))), ((int)(((byte)(111)))));
            this.teamColorButton14.UsePreset = DAEnerys.TeamColorButton.PresetColors.MP1;
            this.teamColorButton14.UseVisualStyleBackColor = false;
            this.teamColorButton14.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton13
            // 
            this.teamColorButton13.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton13.Location = new System.Drawing.Point(220, 94);
            this.teamColorButton13.Name = "teamColorButton13";
            this.teamColorButton13.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton13.StripeColor = System.Drawing.Color.Yellow;
            this.teamColorButton13.TabIndex = 146;
            this.teamColorButton13.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(160)))));
            this.teamColorButton13.UsePreset = DAEnerys.TeamColorButton.PresetColors.HiigaranElite;
            this.teamColorButton13.UseVisualStyleBackColor = false;
            this.teamColorButton13.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton12
            // 
            this.teamColorButton12.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton12.Location = new System.Drawing.Point(196, 94);
            this.teamColorButton12.Name = "teamColorButton12";
            this.teamColorButton12.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton12.StripeColor = System.Drawing.Color.Red;
            this.teamColorButton12.TabIndex = 145;
            this.teamColorButton12.TeamColor = System.Drawing.Color.Black;
            this.teamColorButton12.UsePreset = DAEnerys.TeamColorButton.PresetColors.KithSoban;
            this.teamColorButton12.UseVisualStyleBackColor = false;
            this.teamColorButton12.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton11
            // 
            this.teamColorButton11.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton11.Location = new System.Drawing.Point(172, 94);
            this.teamColorButton11.Name = "teamColorButton11";
            this.teamColorButton11.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton11.StripeColor = System.Drawing.Color.White;
            this.teamColorButton11.TabIndex = 144;
            this.teamColorButton11.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(177)))), ((int)(((byte)(142)))));
            this.teamColorButton11.UsePreset = DAEnerys.TeamColorButton.PresetColors.TanisDefense;
            this.teamColorButton11.UseVisualStyleBackColor = false;
            this.teamColorButton11.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // buttonTeamColorSwap
            // 
            this.buttonTeamColorSwap.Location = new System.Drawing.Point(270, 69);
            this.buttonTeamColorSwap.Name = "buttonTeamColorSwap";
            this.buttonTeamColorSwap.Size = new System.Drawing.Size(66, 22);
            this.buttonTeamColorSwap.TabIndex = 143;
            this.buttonTeamColorSwap.Text = "Swap";
            this.buttonTeamColorSwap.UseVisualStyleBackColor = true;
            this.buttonTeamColorSwap.Click += new System.EventHandler(this.buttonTeamColorSwap_Click);
            // 
            // teamColorButtonCustom
            // 
            this.teamColorButtonCustom.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButtonCustom.Location = new System.Drawing.Point(244, 118);
            this.teamColorButtonCustom.Name = "teamColorButtonCustom";
            this.teamColorButtonCustom.Size = new System.Drawing.Size(20, 20);
            this.teamColorButtonCustom.StripeColor = System.Drawing.Color.SpringGreen;
            this.teamColorButtonCustom.TabIndex = 142;
            this.teamColorButtonCustom.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(255)))));
            this.teamColorButtonCustom.UsePreset = DAEnerys.TeamColorButton.PresetColors.Custom;
            this.teamColorButtonCustom.UseVisualStyleBackColor = false;
            this.teamColorButtonCustom.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButtonDefault
            // 
            this.teamColorButtonDefault.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButtonDefault.Location = new System.Drawing.Point(76, 70);
            this.teamColorButtonDefault.Name = "teamColorButtonDefault";
            this.teamColorButtonDefault.Size = new System.Drawing.Size(20, 20);
            this.teamColorButtonDefault.StripeColor = System.Drawing.Color.SpringGreen;
            this.teamColorButtonDefault.TabIndex = 141;
            this.teamColorButtonDefault.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(255)))));
            this.teamColorButtonDefault.UsePreset = DAEnerys.TeamColorButton.PresetColors.Default;
            this.teamColorButtonDefault.UseVisualStyleBackColor = false;
            this.teamColorButtonDefault.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton10
            // 
            this.teamColorButton10.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton10.Location = new System.Drawing.Point(148, 94);
            this.teamColorButton10.Name = "teamColorButton10";
            this.teamColorButton10.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton10.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.teamColorButton10.TabIndex = 140;
            this.teamColorButton10.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.teamColorButton10.UsePreset = DAEnerys.TeamColorButton.PresetColors.VaygrSP;
            this.teamColorButton10.UseVisualStyleBackColor = false;
            this.teamColorButton10.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton9
            // 
            this.teamColorButton9.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton9.Location = new System.Drawing.Point(124, 94);
            this.teamColorButton9.Name = "teamColorButton9";
            this.teamColorButton9.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton9.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.teamColorButton9.TabIndex = 139;
            this.teamColorButton9.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(141)))), ((int)(((byte)(170)))));
            this.teamColorButton9.UsePreset = DAEnerys.TeamColorButton.PresetColors.Hiigaran;
            this.teamColorButton9.UseVisualStyleBackColor = false;
            this.teamColorButton9.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton8
            // 
            this.teamColorButton8.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton8.Location = new System.Drawing.Point(100, 94);
            this.teamColorButton8.Name = "teamColorButton8";
            this.teamColorButton8.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton8.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.teamColorButton8.TabIndex = 138;
            this.teamColorButton8.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.teamColorButton8.UsePreset = DAEnerys.TeamColorButton.PresetColors.KithSjet;
            this.teamColorButton8.UseVisualStyleBackColor = false;
            this.teamColorButton8.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton7
            // 
            this.teamColorButton7.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton7.Location = new System.Drawing.Point(76, 94);
            this.teamColorButton7.Name = "teamColorButton7";
            this.teamColorButton7.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton7.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(111)))), ((int)(((byte)(29)))));
            this.teamColorButton7.TabIndex = 137;
            this.teamColorButton7.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(173)))), ((int)(((byte)(83)))));
            this.teamColorButton7.UsePreset = DAEnerys.TeamColorButton.PresetColors.KithNabaal;
            this.teamColorButton7.UseVisualStyleBackColor = false;
            this.teamColorButton7.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton6
            // 
            this.teamColorButton6.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton6.Location = new System.Drawing.Point(244, 70);
            this.teamColorButton6.Name = "teamColorButton6";
            this.teamColorButton6.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton6.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(247)))), ((int)(((byte)(101)))));
            this.teamColorButton6.TabIndex = 136;
            this.teamColorButton6.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(151)))), ((int)(((byte)(8)))));
            this.teamColorButton6.UsePreset = DAEnerys.TeamColorButton.PresetColors.KithManaan;
            this.teamColorButton6.UseVisualStyleBackColor = false;
            this.teamColorButton6.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton5
            // 
            this.teamColorButton5.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton5.Location = new System.Drawing.Point(220, 70);
            this.teamColorButton5.Name = "teamColorButton5";
            this.teamColorButton5.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton5.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.teamColorButton5.TabIndex = 135;
            this.teamColorButton5.TeamColor = System.Drawing.Color.White;
            this.teamColorButton5.UsePreset = DAEnerys.TeamColorButton.PresetColors.TaiidanLoyalist;
            this.teamColorButton5.UseVisualStyleBackColor = false;
            this.teamColorButton5.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton4
            // 
            this.teamColorButton4.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton4.Location = new System.Drawing.Point(196, 70);
            this.teamColorButton4.Name = "teamColorButton4";
            this.teamColorButton4.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton4.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(20)))));
            this.teamColorButton4.TabIndex = 134;
            this.teamColorButton4.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(237)))), ((int)(((byte)(37)))));
            this.teamColorButton4.UsePreset = DAEnerys.TeamColorButton.PresetColors.TaiidanImperialist;
            this.teamColorButton4.UseVisualStyleBackColor = false;
            this.teamColorButton4.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton3
            // 
            this.teamColorButton3.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton3.Location = new System.Drawing.Point(172, 70);
            this.teamColorButton3.Name = "teamColorButton3";
            this.teamColorButton3.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton3.StripeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.teamColorButton3.TabIndex = 133;
            this.teamColorButton3.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.teamColorButton3.UsePreset = DAEnerys.TeamColorButton.PresetColors.Beast;
            this.teamColorButton3.UseVisualStyleBackColor = false;
            this.teamColorButton3.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton2
            // 
            this.teamColorButton2.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton2.Location = new System.Drawing.Point(148, 70);
            this.teamColorButton2.Name = "teamColorButton2";
            this.teamColorButton2.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton2.StripeColor = System.Drawing.Color.White;
            this.teamColorButton2.TabIndex = 132;
            this.teamColorButton2.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(108)))), ((int)(((byte)(202)))));
            this.teamColorButton2.UsePreset = DAEnerys.TeamColorButton.PresetColors.Somtaaw;
            this.teamColorButton2.UseVisualStyleBackColor = false;
            this.teamColorButton2.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // teamColorButton1
            // 
            this.teamColorButton1.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton1.Location = new System.Drawing.Point(124, 70);
            this.teamColorButton1.Name = "teamColorButton1";
            this.teamColorButton1.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton1.StripeColor = System.Drawing.Color.Red;
            this.teamColorButton1.TabIndex = 131;
            this.teamColorButton1.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.teamColorButton1.UsePreset = DAEnerys.TeamColorButton.PresetColors.Taiidan;
            this.teamColorButton1.UseVisualStyleBackColor = false;
            this.teamColorButton1.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // buttonStripeColor
            // 
            this.buttonStripeColor.BackColor = System.Drawing.Color.Red;
            this.buttonStripeColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStripeColor.Location = new System.Drawing.Point(76, 43);
            this.buttonStripeColor.Name = "buttonStripeColor";
            this.buttonStripeColor.Size = new System.Drawing.Size(261, 20);
            this.buttonStripeColor.TabIndex = 129;
            this.buttonStripeColor.UseVisualStyleBackColor = false;
            this.buttonStripeColor.Click += new System.EventHandler(this.buttonStripeColor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 128;
            this.label2.Text = "Stripe color";
            // 
            // buttonTeamColor
            // 
            this.buttonTeamColor.BackColor = System.Drawing.Color.Red;
            this.buttonTeamColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTeamColor.Location = new System.Drawing.Point(76, 19);
            this.buttonTeamColor.Name = "buttonTeamColor";
            this.buttonTeamColor.Size = new System.Drawing.Size(261, 20);
            this.buttonTeamColor.TabIndex = 127;
            this.buttonTeamColor.UseVisualStyleBackColor = false;
            this.buttonTeamColor.Click += new System.EventHandler(this.buttonTeamColor_Click);
            // 
            // teamColorButton0
            // 
            this.teamColorButton0.BackColor = System.Drawing.Color.DarkGray;
            this.teamColorButton0.Location = new System.Drawing.Point(100, 70);
            this.teamColorButton0.Name = "teamColorButton0";
            this.teamColorButton0.Size = new System.Drawing.Size(20, 20);
            this.teamColorButton0.StripeColor = System.Drawing.Color.White;
            this.teamColorButton0.TabIndex = 130;
            this.teamColorButton0.TeamColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(159)))), ((int)(((byte)(161)))));
            this.teamColorButton0.UsePreset = DAEnerys.TeamColorButton.PresetColors.Kushan;
            this.teamColorButton0.UseVisualStyleBackColor = false;
            this.teamColorButton0.Click += new System.EventHandler(this.buttonTeamColorPreset_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 126;
            this.label3.Text = "Team color";
            // 
            // colorDialog
            // 
            this.colorDialog.AnyColor = true;
            this.colorDialog.Color = System.Drawing.Color.Gray;
            this.colorDialog.FullOpen = true;
            this.colorDialog.SolidColorOnly = true;
            // 
            // groupUpdates
            // 
            this.groupUpdates.AutoSize = true;
            this.groupUpdates.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupUpdates.Controls.Add(this.checkCheckForUpdates);
            this.groupUpdates.Location = new System.Drawing.Point(352, 458);
            this.groupUpdates.Name = "groupUpdates";
            this.groupUpdates.Size = new System.Drawing.Size(163, 55);
            this.groupUpdates.TabIndex = 21;
            this.groupUpdates.TabStop = false;
            this.groupUpdates.Text = "Updates";
            // 
            // checkCheckForUpdates
            // 
            this.checkCheckForUpdates.AutoSize = true;
            this.checkCheckForUpdates.Location = new System.Drawing.Point(6, 19);
            this.checkCheckForUpdates.Name = "checkCheckForUpdates";
            this.checkCheckForUpdates.Size = new System.Drawing.Size(151, 17);
            this.checkCheckForUpdates.TabIndex = 0;
            this.checkCheckForUpdates.Text = "Check for updates on start";
            this.checkCheckForUpdates.UseVisualStyleBackColor = true;
            this.checkCheckForUpdates.CheckedChanged += new System.EventHandler(this.checkCheckForUpdates_CheckedChanged);
            // 
            // colorDialogAlpha
            // 
            this.colorDialogAlpha.AnyColor = true;
            this.colorDialogAlpha.Color = System.Drawing.Color.Gray;
            this.colorDialogAlpha.FullOpen = true;
            // 
            // labelDockpathSize
            // 
            this.labelDockpathSize.AutoSize = true;
            this.labelDockpathSize.Location = new System.Drawing.Point(8, 99);
            this.labelDockpathSize.Name = "labelDockpathSize";
            this.labelDockpathSize.Size = new System.Drawing.Size(75, 13);
            this.labelDockpathSize.TabIndex = 27;
            this.labelDockpathSize.Text = "Dockpath size";
            // 
            // numericDockpathSize
            // 
            this.numericDockpathSize.DecimalPlaces = 3;
            this.numericDockpathSize.Location = new System.Drawing.Point(105, 97);
            this.numericDockpathSize.Maximum = new decimal(new int[] {
            658067456,
            1164,
            0,
            0});
            this.numericDockpathSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericDockpathSize.Name = "numericDockpathSize";
            this.numericDockpathSize.Size = new System.Drawing.Size(218, 20);
            this.numericDockpathSize.TabIndex = 26;
            this.numericDockpathSize.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericDockpathSize.ValueChanged += new System.EventHandler(this.numericDockpathSize_ValueChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(803, 640);
            this.Controls.Add(this.groupUpdates);
            this.Controls.Add(this.groupRace);
            this.Controls.Add(this.groupRendering);
            this.Controls.Add(this.groupDataPaths);
            this.Controls.Add(this.groupEditor);
            this.Controls.Add(this.groupCamera);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.Text = "Settings";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.numericFarClip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericZoomSpeed)).EndInit();
            this.groupCamera.ResumeLayout(false);
            this.groupCamera.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFOV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNearClip)).EndInit();
            this.groupEditor.ResumeLayout(false);
            this.groupEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRotationIncrement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPositionIncrement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMarkerSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJointSize)).EndInit();
            this.groupDataPaths.ResumeLayout(false);
            this.groupRendering.ResumeLayout(false);
            this.groupRendering.PerformLayout();
            this.groupRace.ResumeLayout(false);
            this.groupRace.PerformLayout();
            this.groupUpdates.ResumeLayout(false);
            this.groupUpdates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDockpathSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelJointSize;
        private System.Windows.Forms.Label labelZoomSpeed;
        private System.Windows.Forms.Label labelMarkerSize;
        private System.Windows.Forms.Label labelFarClip;
        private System.Windows.Forms.NumericUpDown numericFarClip;
        private System.Windows.Forms.NumericUpDown numericZoomSpeed;
        private System.Windows.Forms.GroupBox groupCamera;
        private System.Windows.Forms.GroupBox groupEditor;
        private System.Windows.Forms.NumericUpDown numericMarkerSize;
        private System.Windows.Forms.NumericUpDown numericJointSize;
        private System.Windows.Forms.Label labelNearClip;
        private System.Windows.Forms.NumericUpDown numericNearClip;
        private System.Windows.Forms.Label labelAmbientColor;
        private System.Windows.Forms.Button buttonAmbientColor;
        private System.Windows.Forms.Label labelFOV;
        private System.Windows.Forms.NumericUpDown numericFOV;
        private System.Windows.Forms.Button buttonBackgroundColor;
        private System.Windows.Forms.Label labelBackgroundColor;
        private System.Windows.Forms.GroupBox groupDataPaths;
        private System.Windows.Forms.Button buttonRemoveDataPath;
        private System.Windows.Forms.Button buttonAddDataPath;
        private System.Windows.Forms.ListBox listDataPaths;
        private System.Windows.Forms.OpenFileDialog addDataPathDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericIconSize;
        private System.Windows.Forms.Label labelIconSize;
        private System.Windows.Forms.GroupBox groupRendering;
        private System.Windows.Forms.CheckBox checkVSync;
        private System.Windows.Forms.CheckBox checkRenderOnTop;
        private System.Windows.Forms.Label labelFSAASamples;
        private System.Windows.Forms.ComboBox comboFSAASamples;
        private System.Windows.Forms.CheckBox checkDisableLighting;
        private System.Windows.Forms.GroupBox groupRace;
        private System.Windows.Forms.Label labelBadge;
        private System.Windows.Forms.ComboBox comboBadge;
        private TeamColorButton teamColorButton20;
        private TeamColorButton teamColorButton19;
        private TeamColorButton teamColorButton18;
        private TeamColorButton teamColorButton17;
        private TeamColorButton teamColorButton16;
        private TeamColorButton teamColorButton15;
        private TeamColorButton teamColorButton14;
        private TeamColorButton teamColorButton13;
        private TeamColorButton teamColorButton12;
        private TeamColorButton teamColorButton11;
        private System.Windows.Forms.Button buttonTeamColorSwap;
        private TeamColorButton teamColorButtonCustom;
        private TeamColorButton teamColorButtonDefault;
        private TeamColorButton teamColorButton10;
        private TeamColorButton teamColorButton9;
        private TeamColorButton teamColorButton8;
        private TeamColorButton teamColorButton7;
        private TeamColorButton teamColorButton6;
        private TeamColorButton teamColorButton5;
        private TeamColorButton teamColorButton4;
        private TeamColorButton teamColorButton3;
        private TeamColorButton teamColorButton2;
        private TeamColorButton teamColorButton1;
        private System.Windows.Forms.Button buttonStripeColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonTeamColor;
        private TeamColorButton teamColorButton0;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelEngineColor;
        private System.Windows.Forms.Button buttonEngineColor;
        private EngineColorButton engineColorButton0;
        private EngineColorButton engineColorButton1;
        private EngineColorButton engineColorButtonCustom;
        private EngineColorButton engineColorButton5;
        private EngineColorButton engineColorButton4;
        private EngineColorButton engineColorButton3;
        private EngineColorButton engineColorButton2;
        private Opulos.Core.UI.AlphaColorDialog colorDialogAlpha;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.GroupBox groupUpdates;
        private System.Windows.Forms.CheckBox checkCheckForUpdates;
        private System.Windows.Forms.ComboBox comboBackground;
        private System.Windows.Forms.Label labelBackground;
        private System.Windows.Forms.Label labelRotationIncrement;
        private System.Windows.Forms.NumericUpDown numericRotationIncrement;
        private System.Windows.Forms.Label labelPositionIncrement;
        private System.Windows.Forms.NumericUpDown numericPositionIncrement;
        private System.Windows.Forms.CheckBox checkSmoothZooming;
        private System.Windows.Forms.Label labelDockpathSize;
        private System.Windows.Forms.NumericUpDown numericDockpathSize;
    }
}